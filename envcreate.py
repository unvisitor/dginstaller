import subprocess
import os
import sys
import threading

from dotenv import load_dotenv
from pathlib import Path
from multilogging import multilogger

logger = multilogger(name="Dicergirl Installer", payload="ENV Create")
python: str
pip: str
nb: str


def get_this_path():
    if hasattr(sys, "_MEIPASS"):
        this_path = Path(sys.argv[0]).resolve().parent
    else:
        this_path = Path(__file__).resolve().parent
    return this_path


def check_method():
    if hasattr(sys, "_MEIPASS"):
        logger.info(f"处于编译程序运行状态, 当前运行路径: {Path(sys.argv[0]).resolve().parent}.")
    else:
        logger.info("处于脚本运行状态.")


def load_env():
    global python, pip, nb
    get_this_path()
    load_dotenv()
    python_path = get_this_path() / os.getenv("PYTHON_PATH")
    python = str(python_path / "python.exe")
    pip = str(python_path / "Scripts" / "pip.exe")
    nb = str(python_path / "Scripts" / "nb.exe")
    logger.info(f"使用 Python 解释器: {python}")
    logger.info(f"使用 Python 包管理器: {pip}")
    return python, pip, nb


def run(command, output=True, creationflags=subprocess.CREATE_NO_WINDOW):
    logger.info(f"执行指令: {command}")
    progress = subprocess.run(
        command, shell=True, text=True, capture_output=True, creationflags=creationflags
    )
    if progress.returncode == 0:
        poutput = progress.stdout
    else:
        poutput = progress.stderr

    if output:
        print(poutput)
    return


def read_output(stream):
    while True:
        line = str(stream.readline()).strip()
        if line:
            print("Nonebot2:", line)
        else:
            break


def pipe_run(command):
    try:
        process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            shell=True,
        )

        stdout_thread = threading.Thread(target=read_output, args=(process.stdout,))
        stderr_thread = threading.Thread(target=read_output, args=(process.stderr,))

        stdout_thread.start()
        stderr_thread.start()

        stdout_thread.join()
        stderr_thread.join()

        process.wait()

        if process.returncode == 0:
            print("Command executed successfully!")
        else:
            print("Command failed with return code:", process.returncode)

    except subprocess.CalledProcessError as e:
        print("Command execution failed:", e)


def pip_install(package):
    run([python, "-m", "pip", "install", package, "-i", os.getenv("PYPI"), "--upgrade"])

    # run([pip, "install", package, "-i", "https://mirrors.aliyun.com/pypi/simple"])


def pip_upgrade(package):
    run(
        [
            python,
            "-m",
            "pip",
            "install",
            package,
            "-i",
            os.getenv("PYPI_UPGRADE"),
            "--upgrade",
        ]
    )


def nb_install(package):
    run([python, "-m", "nb_cli", "plugin", "install", package])


def setup_dicergirl():
    dicer_girl_dir = Path.home() / ".dicergirl"
    data_dir = dicer_girl_dir / "data"
    log_dir = dicer_girl_dir / "log"

    if not dicer_girl_dir.exists():
        dicer_girl_dir.mkdir(parents=True, exist_ok=True)

    if not data_dir.exists():
        data_dir.mkdir(parents=True, exist_ok=True)

    if not log_dir.exists():
        log_dir.mkdir(parents=True, exist_ok=True)


def setup_venv():
    # cwd = os.getcwd()
    # pip_install("nb-cli")
    # os.chdir(get_this_path() / "oracle_dicer")

    pip_install("watchdog")
    pip_install("dicergirl")
    pip_upgrade("dicergirl")
    pip_install("nonebot-plugin-uvdiviner")
    pip_install("nonebot-plugin-gocqhttp")

    setup_dicergirl()

    # os.chdir(cwd)


def main():
    # create_virtualenv(Path('venv').resolve())

    load_env()
    setup_venv()


if __name__ == "__main__":
    main()
