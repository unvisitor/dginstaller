from nonebot.adapters.onebot.v11 import Adapter as ONEBOT_V11Adapter
from nonebot.adapters.qq.adapter import Adapter as QQ_Adapter

import nonebot
import sys


def main(adapter):
    adapter = QQ_Adapter if adapter == "qq" else ONEBOT_V11Adapter

    nonebot.init()

    driver = nonebot.get_driver()
    driver.register_adapter(adapter)

    nonebot.load_builtin_plugins("echo")
    nonebot.load_from_toml("pyproject.toml")

    nonebot.run()


if __name__ == "__main__":
    adapter = "qq" if not "--onebot" in sys.argv else "nonebot"
    main(adapter)
