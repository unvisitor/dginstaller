from multilogging import multilogger
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from multiprocessing import Process, freeze_support
from typing import List

import sys
import runpy
import time
import os
import subprocess

folders_to_monitor = [Path(__file__).resolve().parent, Path(sys.executable).resolve().parent / "lib" / "site-packages" / "dicergirl"]

logger = multilogger(name="Dicergirl Installer", payload="Monitor")
current_dir = Path(__file__).resolve().parent
exec_dir = current_dir / "bot.py"
MODULE_TO_RELOAD = 'bot'

def run_shell_command(command, output=True, creationflags=subprocess.CREATE_NO_WINDOW):
    logger.info(f"执行指令: {command}")
    progress = subprocess.run(command, shell=True, text=True, capture_output=True, creationflags=creationflags)
    if progress.returncode == 0:
        poutput = progress.stdout
    else:
        poutput = progress.stderr

    if output:
        print(poutput)
    return

class FileModifiedHandler(FileSystemEventHandler):
    def __init__(self):
        super(FileModifiedHandler, self).__init__()
        self.is_modified: bool = False
        self.modified_module: str = ""
        self.modified_file: str = ""

    def on_modified(self, event):
        if not event.is_directory:
            split = os.path.basename(event.src_path).split(".")

            if len(split) == 1:
                return

            if split[1] == "py":
                self.is_modified = True
                self.modified_module = split[0]
                self.modified_file = event.src_path

def monitor_folders(folders, target=None):
    event_handlers: List[FileModifiedHandler] = []
    observers = []

    for folder_path in folders:
        thread = Process(target=target)
        thread.daemon = True

        event_handler = FileModifiedHandler()
        event_handlers.append(event_handler)

        observer = Observer()
        observer.schedule(event_handler, folder_path, recursive=True)
        observers.append(observer)

    for observer in observers:
        try:
            observer.start()
        except FileNotFoundError:
            logger.error("环境配置异常, 看起来 Dicergirl 似乎没有被正确安装, 请重新安装 DGI 或联系开发者 https://gitee.com/unvisitor/dginstaller/issues.")
            break
        except Exception as error:
            logger.exception(error)
            return

    logger.success("文件监视器已启动, Dicergirl Installer 已开启热重载模式.")
    logger.info("监视目录: " + ', '.join(str(folder) for folder in folders))

    thread.start()

    try:
        while True:
            for event_handler in event_handlers:
                if event_handler.is_modified:
                    event_handler.is_modified = False
                    if target:
                        if thread.is_alive():
                            logger.info(f"文件`{event_handler.modified_file}`被更改, 开始终止主程序.")
                            thread.terminate()
                            run_shell_command(["taskkill", "-f", "-im", "go-cqhttp.exe"], output=False)
                            run_shell_command(["taskkill", "-f", "-im", "nb.exe"], output=False)
                            thread.join()
                        logger.success("主线程已终止, 重启中...")
                        thread = Process(target=target)
                        thread.daemon = True
                        thread.start()
                    else:
                        raise ValueError("监视线程未传入.")
                time.sleep(0.5)
    except KeyboardInterrupt:
        logger.info("用户要求结束任务, 程序退出.")
        for observer in observers:
            observer.stop()

        thread.terminate()
        thread.join()
        sys.exit()

def run():
    sys.path.insert(0, str(current_dir))
    runpy.run_module(MODULE_TO_RELOAD, run_name="__main__", alter_sys=True)

def main():
    logger.info(f"Dicer Girl Installer")
    logger.info("Copyright © 2011-2023 Unknown Visitor. All Rights Reserved.")
    logger.info("开始初始化环境...")
    freeze_support()
    try:
        monitor_folders(folders_to_monitor, target=run)
    except KeyboardInterrupt:
        logger.info("用户要求退出.")
        sys.exit()
    except Exception as e:
        logger.exception(e)

    sys.exit()

if __name__ == "__main__":
    main()