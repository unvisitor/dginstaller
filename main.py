from envcreate import get_this_path, load_env, setup_venv, check_method, run, pipe_run
from pathlib import Path
from multilogging import multilogger
from threading import Thread

import os
import time
import socket
import requests

logger = multilogger(name="Dicergirl Installer", payload="MAIN")
nb: str
python: str
port: str
lock: bool = True
this_path: Path = get_this_path()


def randport() -> int:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("localhost", 0))
    _, port = s.getsockname()
    s.close()
    return port


def run_nb():
    global lock
    pipe_run([python, "run.py"])
    lock = False


def run_nb_thread():
    logger.info("开始启动 NB 监视线程.")
    runner = run_nb
    thread = Thread(target=runner)
    thread.daemon = True
    thread.start()


def check():
    logger.info("检查启动状态中...")
    while True:
        try:
            get = requests.get(f"http://127.0.0.1:{port}/go-cqhttp")
        except:
            continue

        if get.status_code == 200:
            logger.critical(f"欧若可已在 http://127.0.0.1:{port}/ 运行监听.")
            logger.critical(f"打开链接 http://127.0.0.1:{port}/go-cqhttp/ 以配置 QQ 账号.")
            break


def start():
    global nb, port, python
    check_method()

    port = randport()
    logger.info("启动 Dicergirl Installer 中...")

    python, _, nb = load_env()
    setup_venv()

    dg = this_path / "oracle_dicer"
    prod = dg / ".env.prod"
    oldenv = prod.open("r").readline()
    with prod.open("w") as file:
        file.write(oldenv + f"\nHOST=0.0.0.0\nPORT={port}")

    os.chdir(dg)
    run_nb_thread()

    time.sleep(3)
    check()

    while lock:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            logger.info("清理内存中...")
            run(["taskkill", "-f", "-im", "nb.exe"], output=False)
            run(["taskkill", "-f", "-im", "python.exe"], output=False)
            run(["taskkill", "-f", "-im", "go-cqhttp.exe"], output=False)
            logger.success("清理完毕, 回车键退出.")
            input()


def main():
    try:
        start()
    except Exception as error:
        logger.exception(error)
        input()


if __name__ == "__main__":
    main()
